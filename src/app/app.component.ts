import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Result} from './app.result';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'git-viewer';
  constructor(private http: HttpClient) { }
  BaseUrl = "https://api.github.com/search/repositories";
  Url = "";
  search = "";
  repos;
  repo;
  details= false;
  baseReadMeURL = "https://api.github.com/search/repositories/repos/";

  requestData()
  {
    if(this.search != ""){
      this.Url = this.BaseUrl + "?q=" + this.search;
      console.log(this.Url);
      this.getResults().subscribe((result: any) =>  this.updateData(result));
    }
  }

  onKey(event: KeyboardEvent) { // with type info
    this.search = (<HTMLInputElement>event.target).value;
  }

  updateData(result)
  {
    this.repos = result["items"];
    console.log(this.repos);
  }

  displayDetails(repo)
  {
    this.details= true;
    this.repo = repo;
    this.repo.readme = "";
    console.log(this.repo);
    this.requestReadMe();
  }

  back()
  {
    this.details= false;
  }
  

  getResults() {
    return this.http.get(this.Url);
  }

  requestReadMe()
  {
    this.getReadMe().subscribe((result: any) =>  this.updateReadMe(result));
  }

  updateReadMe(result)
  {
    this.repo.readme = result;
  }
  
  getReadMe()
  {
    var re = "https://";
    var str = this.repo.html_url;
    var newstr = str.replace(re, "https://raw.");
    newstr = newstr.replace("github","githubusercontent")
    let url = newstr + "/" + "master/README.md"
    return this.http.get(url ,{responseType: 'text'});
  }


  ngOnInit() 
  { 
    this.requestData();
    //start up logic
  }
}
